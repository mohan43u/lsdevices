namespace Ilugc {
	[GtkTemplate (ui = "/org/Ilugc/LsDevices/resources/ui/lsdevices.ui")]
	class LsDevices : Gtk.Window {
		GUdev.Client uclient;
		Gee.HashMap<string, GUdev.Device> devices;
		[GtkChild]
			Gtk.ComboBoxText DevicesList;
		[GtkChild]
			Gtk.Entry SubSystem;
		[GtkChild]
			Gtk.Entry DevType;
		[GtkChild]
			Gtk.Entry Name;
		[GtkChild]
			Gtk.Entry Number;
		[GtkChild]
			Gtk.Entry SysfsPath;
		[GtkChild]
			Gtk.Entry Driver;
		[GtkChild]
			Gtk.Entry RecentAction;
		[GtkChild]
			Gtk.Entry SeqNum;
		[GtkChild]
			Gtk.Entry DeviceType;
		[GtkChild]
			Gtk.Entry DeviceNumber;
		[GtkChild]
			Gtk.Entry DeviceFile;
		[GtkChild]
			Gtk.Entry SymLinks;
		[GtkChild]
			Gtk.TextView Properties;
		[GtkChild]
			Gtk.TextView SysfsAttrs;
		
		public LsDevices() {
			uclient = new GUdev.Client({});
			devices = new Gee.HashMap<string, GUdev.Device>();
		}

		public void initialize_ui() {
			this.DevicesList.changed.connect(this.assign_values);
			this.set_default_size(640, 480);
			this.set_position(Gtk.WindowPosition.CENTER);
		}

		public void assign_values() {
			string sysfspath = this.DevicesList.get_active_text();
			if(devices.has_key(sysfspath)) {
				GUdev.Device device = devices.get(sysfspath);
				this.SubSystem.set_text(device.get_subsystem() ?? "");
				this.DevType.set_text(device.get_devtype() ?? "");
				this.Name.set_text(device.get_name() ?? "");
				this.Number.set_text(device.get_number() ?? "");
				this.SysfsPath.set_text(device.get_sysfs_path() ?? "");
				this.Driver.set_text(device.get_driver() ?? "");
				this.RecentAction.set_text(device.get_action() ?? "");
				this.SeqNum.set_text(device.get_seqnum().to_string() ?? "");
				this.DeviceType.set_text(device.get_device_type().to_string() ?? "");
				this.DeviceNumber.set_text(((int) device.get_device_number()).to_string() ?? "");
				this.DeviceFile.set_text(device.get_device_file() ?? "");
				this.SymLinks.set_text(string.joinv(",", device.get_device_file_symlinks()) ?? "");
				StringBuilder properties = new StringBuilder();
				foreach(string property in device.get_property_keys()) {
					string propertyvalue = device.get_property(property);
					if(propertyvalue != null) properties.append(property + "=" + propertyvalue + "\n");
				}
				this.Properties.get_buffer().set_text(properties.str ?? "");
				StringBuilder sysfsattrs = new StringBuilder();
				foreach(string sysfsattr in device.get_sysfs_attr_keys()) {
					string sysfsattrvalue = device.get_sysfs_attr(sysfsattr);
					if(sysfsattrvalue != null) sysfsattrs.append(sysfsattr + "=" + sysfsattrvalue + "\n");
				}
				this.SysfsAttrs.get_buffer().set_text(sysfsattrs.str ?? "");
			}
			else {
				this.SubSystem.set_text("");
				this.DevType.set_text("");
				this.Name.set_text("");
				this.Number.set_text("");
				this.SysfsPath.set_text("");
				this.Driver.set_text("");
				this.RecentAction.set_text("removed");
				this.SeqNum.set_text("");
				this.DeviceType.set_text("");
				this.DeviceNumber.set_text("");
				this.DeviceFile.set_text("");
				this.SymLinks.set_text("");
				this.Properties.get_buffer().set_text("");
				this.SysfsAttrs.get_buffer().set_text("");
			}
		}
		
		public void device_cb(string action, GUdev.Device device) {
			string sysfspath = device.get_sysfs_path();
			if(action == "add") {
				devices.set(sysfspath, device);
				this.DevicesList.append_text(sysfspath);
			}
			else if(action == "remove") {
				devices.unset(sysfspath);
			}
		}

		public void run() {
			foreach(GUdev.Device device in uclient.query_by_subsystem(null)) this.device_cb("add", device);
			this.DevicesList.set_active(0);
			uclient.uevent.connect(this.device_cb);
			this.destroy.connect(mainloop.quit);
			this.show_all();
		}
		
		public static LsDevices create(string[] args) {
			Gtk.init(ref args);
			LsDevices lsdevices = new LsDevices();
			return lsdevices;
		}
	}
}

MainLoop mainloop;
int main(string[] args) {
	mainloop = new MainLoop();
	Ilugc.LsDevices lsdevices = Ilugc.LsDevices.create(args);
	lsdevices.initialize_ui();
	lsdevices.run();
	mainloop.run();
	return 0;
}