all: lsdevices

resources.c : resources.xml
	glib-compile-resources --target=resources.c --generate-source resources.xml

lsdevices: lsdevices.vala resources.c
	valac --target-glib=2.38 --pkg=gtk+-3.0 --pkg=gudev-1.0 --pkg=gee-0.8 --gresources=resources.xml lsdevices.vala resources.c

clean:
	rm -fr lsdevices resources.c

.PHONY: clean
